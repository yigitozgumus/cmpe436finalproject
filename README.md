# README #

## Setup Instructions

*  ### Server side Instructions
  * Install node.js to your server [install Link](https://nodejs.org/en/) (Latest version is 7.2.1)
  * Install socket.io library through [here](https://github.com/socketio/socket.io)
  * Server I built uses some parts of the main library of this framework. 
To install your own server 
create a folder in the examples folder and put your server.js file and package.json here.

  * You can find the commands you need below.
```
#!bash

cd socket.io
npm install //installs the necessary packages
cd examples/<your server>
npm install // installs your server's necessary packages
node <your_server>.js
```
* ### Android App Instructions
* Once your node.js server is up and running, you can add the address of your server and the port it is connected to with <IP_ADDRESS:PORT> format to the Constants.java.
 